import { Bonus } from "./Bonus";

export class BoostOnePlayerBonus extends Bonus {

  playerId;

  constructor (bonusData = {}) {
    super({
      name: "McDo+",
      value: "boostOnePlayer",
      icon: "/img/bonus-images/mcdo.png",
      description: "+1 point pour le titulaire de ton choix. En voilà un qui a faim de victoire ! 🤤 (excepté le capitaine).",
      timing: "before",
      isLiveApplied: false,
    });
    this.playerId = bonusData.playerId;
  }

  apply (team) {
    team.pitchPlayers.find(player => player.playerId === this.playerId).bonusRating += 1;
  }
}