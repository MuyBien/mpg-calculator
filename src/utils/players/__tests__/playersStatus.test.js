import mockMatch from "@/assets/mocks/match/response";
import { matchConstructor } from "@/utils/constructors/matchConstructor";
import { beforeEach, describe, expect, it } from "vitest";
import { setMatchPlayersStatus } from "../playersStatus";

describe("Le setter de statut de note", () => {

  let match;
  let championshipMatches;

  beforeEach(() => {
    match = matchConstructor(mockMatch);
  });

  describe("quand le match du joueur est en cours", () => {

    beforeEach(() => {
      championshipMatches = {
        1: {
          period: "firstHalf", // en cours
          home: {
            clubId: 5,
            players: {},
            substitutions: [],
          },
          away: {
            clubId: 6,
            players: {},
            substitutions: [],
          },
        },
      };
    });

    describe("quand le joueur est remplacé", () => {

      beforeEach(() => {
        championshipMatches[1].home.substitutions.push({ subOff: match.homeTeam.pitchPlayers[0].playerId });
        match.homeTeam.pitchPlayers[0].clubId = 5;
      });

      it("ne définit pas sa note en 'live'", async () => {
        const resultMatch = await setMatchPlayersStatus(match, championshipMatches);

        const testPlayer = resultMatch.homeTeam.pitchPlayers[0];
        expect(testPlayer.isLiveRating).toBeFalsy();
      });
    });

    describe("quand le joueur est remplaçant", () => {

      beforeEach(() => {
        championshipMatches[1].home.players = {};
        championshipMatches[1].home.players["mpg_championship_player_111111"] = {
          id: "mpg_championship_player_111111",
          sub: 1,
        };
        match.homeTeam.pitchPlayers[0].playerId = "mpg_championship_player_111111";
        match.homeTeam.pitchPlayers[0].clubId = 5;
      });

      describe("que l'équipe peut encore faire entrer le joueur", () => {

        beforeEach(() => {
          for (let i = 0; i < 4; i ++) { // 4 joueurs déjà remplacés
            championshipMatches[1].home.players[`mpg_championship_player_11111${i}`] = {
              id: `mpg_championship_player_11111${i}`,
              sub: 1,
            };
          }
        });

        it("définit sa note en 'live'", async () => {

          const resultMatch = await setMatchPlayersStatus(match, championshipMatches);

          const testPlayer = resultMatch.homeTeam.pitchPlayers[0];
          expect(testPlayer.isLiveRating).toBeTruthy();
          expect(testPlayer.isLiveSubstitute).toBeTruthy();
        });
      });

      describe("que l'équipe ne peut plus faire entrer le joueur", () => {

        beforeEach(() => {
          for (let i = 0; i < 5; i ++) { // 5 joueurs déjà remplacés
            championshipMatches[1].home.players[`mpg_championship_player_11111${i}`] = {
              id: `mpg_championship_player_11111${i}`,
              sub: 1,
            };
          }
        });

        it("ne définit pas sa note en 'live'", async () => {
          const resultMatch = await setMatchPlayersStatus(match, championshipMatches);

          const testPlayer = resultMatch.homeTeam.pitchPlayers[0];
          expect(testPlayer.isLiveRating).toBeFalsy();
        });

        it("définit sa note en remplaçant final", async () => {
          const resultMatch = await setMatchPlayersStatus(match, championshipMatches);

          const testPlayer = resultMatch.homeTeam.pitchPlayers[0];
          expect(testPlayer.isFinalSubstitute).toBeTruthy();
        });
      });

      describe("que le joueur est finalement rentré", () => {

        beforeEach(() => {
          championshipMatches[1].home.substitutions = [{
            subOn: match.homeTeam.pitchPlayers[0].playerId,
            subOff: "mpg_championship_player_11114514",
          }];
        });

        it("définit sa note en 'live'", async () => {
          const resultMatch = await setMatchPlayersStatus(match, championshipMatches);

          const testPlayer = resultMatch.homeTeam.pitchPlayers[0];
          expect(testPlayer.isLiveRating).toBeTruthy();
          expect(testPlayer.isLiveSubstitute).toBeFalsy();
        });
      });

    });

    describe("que le joueur n'est pas remplacé", () => {

      beforeEach(() => {
        championshipMatches[1].home.substitutions = [];
        match.homeTeam.pitchPlayers[0].clubId = 5;
      });

      it("définit sa note en 'live'", async () => {
        const resultMatch = await setMatchPlayersStatus(match, championshipMatches);

        const testPlayer = resultMatch.homeTeam.pitchPlayers[0];
        expect(testPlayer.isLiveRating).toBeTruthy();
      });
    });
  });

  describe("quand le match du joueur est fini", () => {

    beforeEach(() => {
      championshipMatches = {
        1: {
          period: "fullTime", // terminé
          home: {
            clubId: 3,
            players: {},
            substitutions: [],
          },
          away: {
            clubId: 4,
            players: {},
            substitutions: [],
          },
        },
      };
      match.homeTeam.pitchPlayers[0].clubId = 3;
    });

    it("ne définit pas sa note en 'live'", async () => {
      const resultMatch = await setMatchPlayersStatus(match, championshipMatches);

      const testPlayer = resultMatch.homeTeam.pitchPlayers[0];
      expect(testPlayer.isLiveRating).toBeFalsy();
    });
  });

  describe("quand le match du joueur n'a pas commencé", () => {

    beforeEach(() => {
      championshipMatches = {
        1: {
          home: {
            clubId: 1,
            players: {},
          },
          away: {
            clubId: 2,
            players: {},
          },
        },
      };
      match.homeTeam.pitchPlayers[0].clubId = 1;
    });

    it("ne définit pas sa note en 'live'", async () => {
      const resultMatch = await setMatchPlayersStatus(match, championshipMatches);

      const testPlayer = resultMatch.homeTeam.pitchPlayers[0];
      expect(testPlayer.isLiveRating).toBeFalsy();
    });
  });

});